package ru.sd.ok.kotlin

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.web.client.RestTemplate
import ru.sd.ok.kotlin.model.*

class OkApi (val token:String) {
    fun sendMessage(okMessage: OutputOkMessage, chatId: String) : String {
        val template = RestTemplate()
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON_UTF8
        val mapper = ObjectMapper()
        val stringValue = mapper.writeValueAsString(okMessage)
        val request = HttpEntity(stringValue, headers)
        val exchange = template.exchange("https://api.ok.ru/graph/me/messages/$chatId?access_token=$token", HttpMethod.POST, request, String::class.java)
        return exchange.body
    }

    fun sendAttachment(chatId: String, link: String) :String {
        val recipient = Recipient(chatId)
        val payload = Payload(link, null)
        val attachment = OkAttachment(OkAttachmentType.IMAGE, payload)
        val message = Message(null, null, null, null, null, attachment)
        val okMessage = OutputOkMessage(recipient, message, null)
        return sendMessage(okMessage, chatId)
    }

    fun sendText(chatId: String, text: String) :String {
        val recipient = Recipient(chatId)
        val message = Message(text)
        val okMessage = InputOkMessage(null, recipient, message, null)
        val template = RestTemplate()
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON_UTF8
        val mapper = ObjectMapper()
        val stringValue = mapper.writeValueAsString(okMessage)
        val request = HttpEntity(stringValue, headers)
        val exchange = template.exchange("https://api.ok.ru/graph/me/messages/$chatId?access_token=$token", HttpMethod.POST, request, String::class.java)
        return exchange.body
    }
}