package ru.sd.ok.kotlin

import org.springframework.boot.context.properties.ConfigurationProperties


@ConfigurationProperties(prefix = "adapter.ok")
class OkProperties() {
    var webhooktoken: String? = null
    var url: String? = null
}