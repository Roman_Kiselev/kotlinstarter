package ru.sd.ok.kotlin.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.*
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate
import ru.sd.ok.kotlin.model.InputOkMessage
import javax.annotation.PostConstruct

@RestController
@RequestMapping(value = ["/rest/ok/webhook"], produces = [MediaType.APPLICATION_JSON_UTF8_VALUE])
open class OkController {
    @Autowired
    @Qualifier("url")
    lateinit var currentServerUrl:String
    @Autowired
    @Qualifier("webhook")
    lateinit var okWebhookToken:String

    @PostConstruct
    fun registerWebHook(){
        val template = RestTemplate()
        val header = HttpHeaders()
        header.contentType = MediaType.APPLICATION_JSON_UTF8
        val request = HttpEntity("{\"url\":\"$currentServerUrl/rest/ok/webhook\"}", header)
        val exchange = template.exchange("https://api.ok.ru/graph/me/subscribe?access_token=$okWebhookToken", HttpMethod.POST, request, String::class.java)
        println(exchange)
    }

    @PostMapping
    fun messageHandler(@RequestBody inputMessage: InputOkMessage):ResponseEntity<HttpStatus> {
        println(inputMessage)
        return ResponseEntity(HttpStatus.OK)
    }
}