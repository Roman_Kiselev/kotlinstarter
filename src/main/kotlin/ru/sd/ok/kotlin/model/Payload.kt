package ru.sd.ok.kotlin.model

import com.fasterxml.jackson.annotation.JsonInclude

data class Payload(@get:JsonInclude(JsonInclude.Include.NON_NULL)var url: String?,
                   @get:JsonInclude(JsonInclude.Include.NON_NULL)var ref:String?)