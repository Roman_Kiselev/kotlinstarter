package ru.sd.ok.kotlin.model

import com.fasterxml.jackson.annotation.JsonInclude

data class OutputOkMessage (var recipient: Recipient, var message: Message,
                            @get:JsonInclude(JsonInclude.Include.NON_NULL)var okAttachment: OkAttachment?)