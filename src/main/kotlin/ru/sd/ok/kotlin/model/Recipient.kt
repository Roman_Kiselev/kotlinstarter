package ru.sd.ok.kotlin.model

import com.fasterxml.jackson.annotation.JsonProperty

data class Recipient(@get:JsonProperty("chat_id")var chatId: String)