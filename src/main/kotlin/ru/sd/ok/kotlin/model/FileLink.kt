package ru.sd.ok.kotlin.model

import com.fasterxml.jackson.annotation.JsonProperty


data class FileLink (var url: String, @get:JsonProperty("field_id") var fileId: String)