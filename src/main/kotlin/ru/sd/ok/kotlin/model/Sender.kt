package ru.sd.ok.kotlin.model

import com.fasterxml.jackson.annotation.JsonProperty

data class Sender(var name: String, @get:JsonProperty("user_id")var userId: String)