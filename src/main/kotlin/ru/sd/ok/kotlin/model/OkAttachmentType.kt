package ru.sd.ok.kotlin.model

enum class OkAttachmentType {
    IMAGE, FILE, SHARE
}