package ru.sd.ok.kotlin.model

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

data class Message (@get:JsonInclude(JsonInclude.Include.NON_NULL) var text: String?,
                    @get:JsonInclude(JsonInclude.Include.NON_NULL) var seq: Long?,
                    @get:JsonInclude(JsonInclude.Include.NON_NULL) var mid: String?,
                    @get:JsonProperty("reply_to")var replyTo:String?,
                    @get:JsonInclude(JsonInclude.Include.NON_NULL) var attachments: List<OkAttachment>?,
                    @get:JsonInclude(JsonInclude.Include.NON_NULL) var attachment: OkAttachment?) {
    constructor(text: String) : this(text, null, null, null, null, null) {
        this.text = text
    }
}