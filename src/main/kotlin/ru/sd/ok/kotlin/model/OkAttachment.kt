package ru.sd.ok.kotlin.model

data class OkAttachment (var okAttachmentType: OkAttachmentType, var payload: Payload)