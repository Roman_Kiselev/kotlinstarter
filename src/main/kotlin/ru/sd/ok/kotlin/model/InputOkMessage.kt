package ru.sd.ok.kotlin.model

import com.fasterxml.jackson.annotation.JsonInclude
import java.sql.Timestamp

data class InputOkMessage(@get:JsonInclude(JsonInclude.Include.NON_NULL)var sender: Sender?,
                          @get:JsonInclude(JsonInclude.Include.NON_NULL)var recipient: Recipient?,
                          @get:JsonInclude(JsonInclude.Include.NON_NULL)var message: Message?,
                          @get:JsonInclude(JsonInclude.Include.NON_NULL)var timestamp: Timestamp?){
    constructor():this(null, null, null,null)
}