package ru

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import ru.sd.ok.kotlin.OkApi
import ru.sd.ok.kotlin.OkProperties

@Configuration
@EnableConfigurationProperties(OkProperties::class)
@ComponentScan
class OkConfigur {
    @Autowired
    private val okProperties: OkProperties? = null

    @Bean
    fun okApi(okWebhookToken: String): OkApi {
        return OkApi(okWebhookToken)
    }

    @Bean
    @Qualifier("url")
    fun currentServerUrl(): String {
        if (okProperties?.url != null) {
            return okProperties.url!!
        }
        return ""
    }

    @Bean
    @Qualifier("webhook")
    fun okWebhookToken():String {
        if (okProperties?.webhooktoken != null) {
            return okProperties.webhooktoken!!
        }
        return ""
    }
}