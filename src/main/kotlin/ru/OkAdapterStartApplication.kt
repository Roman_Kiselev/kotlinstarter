package ru.utair

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class OkAdapterStartApplication

fun main(args: Array<String>) {
    SpringApplication.run(OkAdapterStartApplication::class.java, *args)
}
